<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <h1>Buat Account Baru!</h1>
</head>

<body>
    <h2>Sign Up Form</h2>
    <form action="{{ url('welcome') }}">
        <p><label for="firstname">First Name:</label> <br>
            <input type="text"></p>
        <p><label for="lastname">Last Name:</label> <br>
            <input type="text"></p>
        <p><label for="gender">Gender: </label> <br>
        </p>
        <input type="radio" id="male" name="rad">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="rad">
        <label for="female"></label>Female <br>
        <input type="radio" id="other" name="rad">
        <label for="other"></label> Other <br>
        <p><label for="nationality">Nationality:</label></p>
        <select name="nationality" id="nationality">
            <option value="indonesian" selected>Indonesian</option>
            <option value="japan">Japan</option>
          </select>
        <br>
        <p><label for="language">Language Spoken:</label></p>
        <input type="checkbox" id="indonesia" name="indonesia" value="Bahasa Indonesia">
        <label for="vehicle1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="English">
        <label for="vehicle2">English</label><br>
        <input type="checkbox" id="others" name="others" value="Others">
        <label for="vehicle3">Others</label>

        <p><label for="bio">Bio:</label></p>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br><br>

        <input type="submit" name="submit" value="Sign Up">
    </form>
</body>

</html>